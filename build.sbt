name := "cats-reloaded"
organization := "com.ingenico"
version := "0.0.1-SNAPSHOT"

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.12"
)