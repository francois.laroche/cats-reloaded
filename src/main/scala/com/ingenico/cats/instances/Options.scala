package com.ingenico.cats.instances

import com.ingenico.cats.flatmap.Flatmap
import com.ingenico.cats.functor.Functor

trait Options {

  implicit object OptionFunctor extends Functor[Option] {
    override def map[A, B](base: Option[A], fun: A => B): Option[B] =
      base.map(fun)
  }

  implicit object OptionFlatMap extends Flatmap[Option] {
    override def flatMap[A, B](self: Option[A], f: A => Option[B]): Option[B] =
      self.flatMap(f)
  }
}
