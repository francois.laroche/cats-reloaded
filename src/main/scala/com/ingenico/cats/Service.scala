package com.ingenico.cats

import com.ingenico.cats.flatmap.Flatmap
import com.ingenico.cats.functor.Functor
import com.ingenico.cats.syntax.all.toFlatMapOps
import com.ingenico.cats.syntax.all.toFunctorOps

class Service[F[_]](implicit functor: Functor[F], flatmap: Flatmap[F]) {

  def doSomething[A, B](input: F[A], f: A => B): F[B] = {
    input.map(f)

    input.flatMap(???)

  }

  def doSomethingElse(): F[Unit] = ???

  for {
    _ <- doSomethingElse()
  } yield ()

}
