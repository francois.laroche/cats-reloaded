package com.ingenico.cats.flatmap

trait Flatmap[F[_]] {
  def flatMap[A, B](self: F[A], f: A => F[B]): F[B]
}

object Flatmap {
  def apply[F[_]](implicit flatmap: Flatmap[F]): Flatmap[F] = flatmap
}
