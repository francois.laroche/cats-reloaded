package com.ingenico.cats.syntax.functor

import com.ingenico.cats.functor.Functor

class FunctorOps[F[_]: Functor, A](self: F[A]) {

  def map[B](f: A => B): F[B] = Functor[F].map(self, f)

}

trait FunctorSyntax {
  implicit def toFunctorOps[F[_]: Functor, T](self: F[T]): FunctorOps[F, T] =
    new FunctorOps[F, T](self)
}
