package com.ingenico.cats.syntax

import com.ingenico.cats.syntax.flatmap.FlatMapSyntax
import com.ingenico.cats.syntax.functor.FunctorSyntax

package object all extends FunctorSyntax with FlatMapSyntax {}
