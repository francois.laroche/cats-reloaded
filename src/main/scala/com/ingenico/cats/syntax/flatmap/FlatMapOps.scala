package com.ingenico.cats.syntax.flatmap

import com.ingenico.cats.flatmap.Flatmap

class FlatMapOps[F[_]: Flatmap, A](self: F[A]) {

  def flatMap[B](f: A => F[B]): F[B] = Flatmap[F].flatMap(self, f)

}

trait FlatMapSyntax {
  implicit def toFlatMapOps[F[_]: Flatmap, T](self: F[T]): FlatMapOps[F, T] =
    new FlatMapOps[F, T](self)
}
