package com.ingenico.cats.functor

trait Functor[F[_]] {
  def map[A, B](base: F[A], fun: A => B): F[B]
}

object Functor {
  def apply[F[_]](implicit functor: Functor[F]): Functor[F] = functor
}
